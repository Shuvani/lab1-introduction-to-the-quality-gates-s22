FROM openjdk:8-jdk-alpine
COPY target/main-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080/tcp
EXPOSE 8080/udp
ENTRYPOINT ["java","-jar","/app.jar"]
